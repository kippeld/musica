from setuptools import setup

setup(
   name='musica',
   version='1.0',
   author_email='kippeld@gmail.com',
   packages=['musica']
)
