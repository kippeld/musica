import argparse
import yaml
import os
import yt_dlp
import eyed3

def u(prime_service):
    for i in prime_service['api']:
        print(i)

def home():
    print("""
    music
    home
    u: text
    q: exit
    """)

################################################################################

def yt(x, c):

    yt_opts = {
      'format': 'bestaudio/best',
      'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192'
      }],
      'postprocessor_args': [
        '-ar', '16000'
      ],
      'prefer_ffmpeg': False,
      'keepvideo': False,
      #'outtmpl' : pista
    }



    if "pista" in c:
        yt_opts['outtmpl'] = c["pista"]

        ydl = yt_dlp.YoutubeDL(yt_opts)
        ydl.download(x)


        audiofile = eyed3.load(c["pista"] + ".mp3")

        if "pista" in c:
            audiofile.tag.title = c["pista"]
        if "album" in c:
            audiofile.tag.artist = c["album"]
        if "album_artist" in c:
            audiofile.tag.album_artist = c["album_artist"]
        if "artist" in c:
            audiofile.tag.artist = c["artist"]
        #audiofile.tag.artist = "Token Entry"
        #audiofile.tag.album = "Free For All Comp LP"
        #audiofile.tag.album_artist = "Various Artists"
        #audiofile.tag.title = pista


        audiofile.tag.save()
        #print(ydl.format)
    else:
        ydl = yt_dlp.YoutubeDL(yt_opts)
        ydl.download(x)


def music(prime_service):
    for i in prime_service["api"]:
        for n in prime_service["api"][i]:
            #print(n["pista"])
            c = {}

            if "pista" in n:
                c["pista"] = n["pista"]
            if "album" in n:
                c["album"] = n["album"]
            if "album_artist" in c:
                c["album_artist"] = n["album_artist"]
            if "artist" in c:
                c["artist"] = n["artist"]

            for id, x in enumerate(n['youtube']):
                if id == 0:
            #        print(id, x)
                    yt(x, c)
                    break


        #print(n)
################################################################################
def red(user):
    with open(user, 'r') as file:
        prime_service = yaml.safe_load(file)
    #print(prime_service)
    u(prime_service)
    while True:
        a = input(': ')
        if a == 'q': break
        elif a == 'u': u(prime_service)
        elif a == 'home': home()
        elif a == 'music':
            music(prime_service)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("-u", "--user", required=True)

    args = parser.parse_args()

    user = args.user

    red(user)
